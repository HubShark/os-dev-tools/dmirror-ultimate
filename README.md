# DMirror Ultimate
This set of scripts was originally <a href="https://code.launchpad.net/~ubumirror-devs/ubumirror/trunk" target="_blank">designed for mirroring Ubuntu</a>. I've decided to change it for use with Debian to see if the errors from reprepro get fixed downloading the mirror in this manor. A lot has changed in the last few years, so I need some catching up.

